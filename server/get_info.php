<?php
	require("./GettyFunctions.php");
	require("./GettyImportFunctions.php");
	$CREDENTIALS_FILE = "creds.json";
	$SESSION_FILE = "session_v3.json";
	if (!isSet($_GET["id"]) || $_GET["id"] === "") {
		echo json_encode( ["status" => "error", "message" => "Need an image ID, pleez"] );
		exit;
	} else {
		$id = $_GET["id"];
	}
	////////////////////////////////////////////////////////////////
	/////////////////////  GET API CREDENTIALS  ////////////////////
	////////////////////////////////////////////////////////////////
	$creds = json_decode( file_get_contents($CREDENTIALS_FILE) );
	////////////////////////////////////////////////////////////////
	/////// CHECK IF WE STILL HAVE AN ACTIVE API SESSION ///////////
	////////////////////////////////////////////////////////////////
	$session = json_decode( file_get_contents($SESSION_FILE) );
	$sessionExpires = (int)$session->expires;
	$now = (int)date("U");
	if ( $now >  $sessionExpires ) {
		$token = createNewSession($creds, $SESSION_FILE);
	} else {
		$token = $session;
	}

	$imageInfo = getImageInfo($creds, $token, $id);

	echo json_encode($imageInfo);
	exit;
