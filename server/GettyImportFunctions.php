<?php
	////////////////////////////////////////////////////////////////
	//////////// FUNCTION TO CREATE NEW API SESSION ////////////////
	////////////////////////////////////////////////////////////////
	function createNewSession($creds, $SESSION_FILE) {
		$authEndpoint = "https://api.gettyimages.com/oauth2/token";

		$curl = getCurlForFormPost($authEndpoint);
		setFormData($curl, array(
			"grant_type" => "client_credentials",
			"client_id" => $creds->system_id, 
			"client_secret" => $creds->system_password
		));
		$resultObj = executeCurl($curl);
		$result = json_decode($resultObj['body'],true);

		// evaluate for success response
		if ($result === false) {
			echo json_encode( ["status" => "error", "message" => "Failed to create api session"] );
			exit;
		}
		$ret = false;
		if ($result) {
	      $ret = new stdClass;
			  $ret->access_token = $result["access_token"];
			  $ret->token_type = $result["token_type"];
	          $ret->expires = date("U") + intval($result["expires_in"]);
		} else {
			echo json_encode( ["status" => "error", "message" => "Failed to create api session"] );
			exit;
		}
		//Save our session info to file so it can be reused for 30 minutes.
		file_put_contents($SESSION_FILE, json_encode($ret));

		return $ret;
	}
	////////////////////////////////////////////////////////////////
	////////////// FUNCTION TO GET IMAGE INFO //////////////////////
	////////////////////////////////////////////////////////////////
	function getImageInfo($creds, $token, $imageId) {
		$metadataEndpoint = "https://api.gettyimages.com/v3/images?";
		$params = "fields=product_types,summary_set,preview,downloads&ids=";
		$curl = getCurl($metadataEndpoint . $params . $imageId);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"Api-Key:" . $creds->system_id,
			"authorization:" . $token->token_type . " " . $token->access_token
		));
		$result = json_decode(executeCurl($curl)['body']);
		if ($result && count($result->images) > 0) {
        	$ret = $result->images[0];
			if ( !in_array("editorialsubscription", $ret->product_types) ){
				echo json_encode( ["status" => "error", "message" => "Not a subscription image"] );
				exit;
			}
		} else {
			echo json_encode( ["status" => "error", "message" => "Failed to get image info"] );
			exit;
		}
		return $ret;
	}
	////////////////////////////////////////////////////////////////
	///////// FUNCTION TO RETRIEVE A DOWNLOAD URL //////////////////
	////////////////////////////////////////////////////////////////
	function getDownloadURL($creds, $tokenType, $token, $imageId) {
		$downloadEndpoint = "https://api.gettyimages.com/v3/downloads/images/" . $imageId . "?auto_download=false";

		$headersToSend = array(CURLOPT_HTTPHEADER => array(
								"Api-Key: " . $creds->system_id,
								"authorization: " . $tokenType . " " . $token),
								CURLOPT_FOLLOWLOCATION => TRUE);
		$curl = getCurlForPost($downloadEndpoint, $headersToSend);
		$rawResponse = executeCurl($curl);
		$response = json_decode($rawResponse["body"]);
		// evaluate for success response
		if ( $response && isSet($response->uri) ) {
			return $response->uri;	
		} else {
			echo json_encode( ["status" => "error", "message" => "Failed to download image"] );
			exit;
		}
	}
	////////////////////////////////////////////////////////////////
	///////////// FUNCTION DOWNLOAD A GETTY IMAGE //////////////////
	////////////////////////////////////////////////////////////////
	function fetchImage($url, $id, $tag, $imageInfo, $FILENAME_TAG, $TMP_PATH) {
		$title = sanitize( $imageInfo->title );
		$title = substr( $title, 0, 25);
		$tag = substr( $tag, 0, 25 );
		$filename = $tag . $title . "_" . $id . $FILENAME_TAG;
		file_put_contents($TMP_PATH . $tag . $title . "_" . $id . $FILENAME_TAG, file_get_contents($url));
		return $filename;
	}
	////////////////////////////////////////////////////////////////
	/////////////// FUNCTION TO SANITIZE FILENAMES /////////////////
	////////////////////////////////////////////////////////////////
	function sanitize($string) {
		$string = str_replace(" ", "_", $string);
		return preg_replace("/[^A-Za-z0-9-_]/", "", $string);
	}
?>
