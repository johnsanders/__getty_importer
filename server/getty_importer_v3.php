<?php
	require("./GettyFunctions.php");
	$GLOBALS['THUMB_HEIGHT'] = 300;
	$GLOBALS['HD_WIDTH'] = 1920;
	$GLOBALS['HD_HEIGHT'] = 1080;
	$GLOBALS['HD_ASPECT'] = $GLOBALS['HD_WIDTH'] / $GLOBALS['HD_HEIGHT'];

	$GLOBALS['NEWSIMAGES_HD_PATH'] = "/mnt/HD_IMAGES_WRITABLE/";
	$GLOBALS['HLN_PATH'] = "/mnt/hln_import/";
	$GLOBALS['IMAGE_PATH'] = "/home/cnnitouch/www/cnnimages/";
	$GLOBALS['IMAGE_URL'] = "/cnnimages/";
	$GLOBALS['TMP_PATH'] = "/home/cnnitouch/www/utilities/getty_importer/tmp/";
	$GLOBALS['TMP_URL'] = "/utilities/getty_importer/tmp/";
	$GLOBALS['BLUR_AMOUNT'] = 50;

	$GLOBALS['SESSION_FILE'] = "session_v3.json";
	$GLOBALS['IMAGE_PATH'] = $IMAGE_PATH;
	$GLOBALS['FILENAME_TAG'] = "_GETTY.jpg";
	$CREDENTIALS_FILE = "creds.json";



	if (isset($_GET["phrase"])){
		$token = array(
			"tokenType" => "",
			"token" => ""
		);
		$creds = json_decode( file_get_contents($CREDENTIALS_FILE) );
		$session = json_decode( file_get_contents($GLOBALS["SESSION_FILE"]) );
		$sessionExpires = (int)$session->expires;
		$now = (int)date("U");
		$phrase = $_GET["phrase"];
		$page = isset($_GET["page"]) ? $_GET["page"] : "1";
		$order = isset($_GET["order"]) ? $_GET["order"] : "most_popular";
		if ( $now >  $sessionExpires ) {
			$token = createNewSession($creds);
		} else {
			$token = $session;
		}
		echo doSearch($creds, $token, $phrase, $order, $page); 
		exit;
	}
	function doSearch($creds, $token, $phrase, $order, $page) {
		$metadataendpoint = "https://api.gettyimages.com/v3/search/images/editorial?";
		$url = $metadataendpoint .
			"fields=summary_set,display_set,date_created" .
			"&product_types=editorialsubscription" .
			"&page_size=50" . 
			"&sort_order=" . urlencode($order) .
			"&page=" . urlencode($page) .
			"&phrase=" . urlencode($phrase);
		$curl = getcurl($url);

		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"api-key:".$creds->system_id,
			"authorization: " . $token->token_type . " " . $token->access_token
		));
		return executecurl($curl)["body"];
	}





	////////////////////////////////////////////////////////////////
	/////  SHOW INPUT FORM IF NO POST DATA SPECIFIED  ///////////////
	/////////////////////////////////////////////////////////////////
	if ( count($_POST) == 0 ) {
		sendHeader();
		?>
		<form action="getty_importer_v3.php" method="post" accept-charset="utf-8" class="form-horizontal">
			<div class="form-group">
				<label class="col-xs-2 col-xs-offset-2 control-label" for="id">Getty Image ID</label>
				<div class="col-xs-6">
					<input class="form-control" name="id" type="text" id="id">
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-xs-2 col-xs-offset-2 control-label">Filename Tag (Optional)<br>So you can find it later</label>
				<div class="col-xs-6">
					<input class="form-control" name="tag" type="text" id="tag">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-offset-4 col-xs-1">
					<input class="btn btn-primary" type="submit" value="Import" id="submit">
				</div>
				<div class="col-xs-3">
					<div id='loading' style='display:none'>
						<span class="loader fa fa-spinner fa-pulse fa-3x fa-fw"></span>
					</div>
				</div>
			</div>
		</form>


		<?php
		sendFooter();
		exit;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//////IF WE GOT AN ID VALUE, WE'RE IN STEP 2. INITIALIZE VARIABLES AND CHECK INPUTS  /////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////
	if ( isSet($_POST['id']) ) {
		$id = trim($_POST['id']);
		$id = str_replace( "#", "", $id );

		if ( isSet($_POST['tag']) && $_POST['tag'] != "" ) {
			$tag = sanitize($_POST['tag']) . "_";
		} else {
			$tag = "";
		}

		$touchscreen = isSet($_GET['touchscreenswitch']);
		$newsimageshd = isSet($_GET['vizhdswitch']);
		$hln = isSet($_GET['hlnswitch']);

		$token = array(
							"tokenType" => "",
							"token" => ""
						);
		$CREDENTIALS_FILE = "creds.json";

		////////////////////////////////////////////////////////////////
		/////////////////////  GET API CREDENTIALS  ////////////////////
		////////////////////////////////////////////////////////////////
		$creds = json_decode( file_get_contents($CREDENTIALS_FILE) );

		////////////////////////////////////////////////////////////////
		/////// CHECK IF WE STILL HAVE AN ACTIVE API SESSION ///////////
		////////////////////////////////////////////////////////////////
		$session = json_decode( file_get_contents($GLOBALS["SESSION_FILE"]) );
		$sessionExpires = (int)$session->expires;
		$now = (int)date("U");
		if ( $now >  $sessionExpires ) {
			$token = createNewSession($creds);
		} else {
			echo "Using existing credentials";
			$token = $session;
		}

		////////////////////////////////////////////////////////////////
		////////////// GET INFO ABOUT IMAGE  ///////////////////////////
		////////////// GET DOWNLOAD URL  ///////////////////////////////
		////////////// USE URL TO FETCH IMAGE  /////////////////////////
		////////////////////////////////////////////////////////////////
		$imageInfo = getImageInfo($creds, $id);

		$downloadUrl = getDownloadURL($creds, $token->token_type, $token->access_token, $id);
		$title = sanitize( $imageInfo->caption );
		$title = substr( $title, 0, 25);
		if (file_exists( $GLOBALS['TMP_PATH'] . $tag . $title . "_" . $id . $GLOBALS['FILENAME_TAG'] )){
			$filename = $tag . $title . "_" . $id . $GLOBALS['FILENAME_TAG'];
		} else {
			$filename = fetchImage($downloadUrl, $id, $tag, $imageInfo);
		}

		if ( file_exists($GLOBALS['TMP_PATH'] . $filename) ) {
			$img = new Imagick( $GLOBALS['TMP_PATH'] . $filename );
			$imageprops = $img->getImageGeometry();
			if ($imageprops['width'] >= $imageprops['height']) {
				$imgDisplayWidth = 800;
				$vertWarning = "";
			} else {
				$imgDisplayWidth = 500;
				$vertWarning = "<div style='color:red; font-weight:bold;'>WARNING: THIS IMAGE IS VERTICAL<br>ONLY THE RED CROPPED AREA BELOW WILL BE SEEN!</div>\n";
			}


			sendHeader();
			?>
			<h3 class=text-center>Almost there... one more step.</h3>
			<?php echo $vertWarning;?>
			<div class="row">
				<div class="col-xs-12 text-center">Here's how the image will be cropped.  The red part is what will be in your image.</div>
				<div class="row">
					<div class="col-xs-8 col-xs-offset-2 well">
						<ul>
							<li>You can move the red crop box by dragging in the center.</li>
							<li>Resize by dragging from the corners.</li>
							<li>Keep in mind what will be covered by the banner and bug.</li>
							<li>When you're ready, choose where you want the image to be saved and click "Save Image"</li>
						</ul>
					</div>
				</div>

			<form action="getty_importer.php" method="post" accept-charset="utf-8">
				<div class="row">
					<div class="col-xs-8 col-xs-offset-2 well">
						<div class="row">
							<div class='col-xs-12 text-center'><strong>SEND TO</strong></div>
						</div>
						<div class="row">
							<div>
								<div class="col-xs-3 awesomeCheckbox checkbox checkbox-primary"><input type=checkbox name="touchscreenswitch" id="touchscreenswitch"/><label for=touchscreenswitch >Touchscreen</label></div>
							</div>
							<div>
								<div class="col-xs-3 awesomeCheckbox checkbox checkbox-primary"><input type=checkbox name="vizhdswitch" id="vizhdswitch"/><label for=vizhdswitch>Viz Newsimages</label></div>
							</div>
							<div>
								<div class="col-xs-3 awesomeCheckbox checkbox checkbox-primary"><input type=checkbox id="hlnswitch" name="hlnswitch"/><label for=hlnswitch >HLN</label></div>
							</div>
							<div>
		                        <div class="col-xs-3 awesomeCheckbox checkbox checkbox-primary"><input type=checkbox id="jpegswitch" name="jpegswitch"/><label for=jpegswitch >Downoad as jpeg</label></div>
							</div>
							<input type='hidden' id="cropx" name="cropx" value='0'/>
							<input type='hidden' id="cropy" name="cropy" value='0.125'/>
							<input type='hidden' id="cropw" name="cropw" value='1'/>
							<input type='hidden' id="filename" name="filename" value="<?php echo $filename;?>">
							<input type='hidden' id="imageid" name="imageid" value="<?php echo $id;?>">
							<input type='hidden' id="tag" name="tag" value="<?php echo $tag;?>">
						</div>
						<div class="row">
							<div class="col-xs-12 text-center">
								<input type="submit" value="Save Image" class="btn btn-default" id="submit">
							</div>
						</div>
					</div>

				</div>

				<div class="row">
					<div id='cropper' style='margin-left:16.66666667%; width:66.66666667%'>
						<img id='image' src='<?php echo $GLOBALS['TMP_URL'] . $filename;?>' width=<?php echo $imgDisplayWidth;?>>
						<div id='resizable'>
							<div id='guide'>
								<div id='guideLeft'>BANNER</div>
								<div id='guideRight'>BUG</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-center">
						<div class="checkbox checkbox-primary">
							<input type=checkbox id='verticalRight' name='verticalRight'/>
							<label for="verticalRight">Flip sideways for vertical monitor?</label>
						</div>
					</div>
				</div>
			<?php
			sendFooter();
			exit;
		} else {
			sendHeader();
			echo "<p>This is just dreadful.  Something went wrong.</p>";
			echo "<p style='margin-top:20px'><a href='/utilities/getty_importer/getty_importer.php'>Go Back</a></p>";
			sendFooter();
			exit;
		}
		exit;
	}

	////////////////////////////////////////////////////////////
	//////IF WE GOT A CROP VALUE, WE'RE IN STEP 3.  ////////////
	////////////////////////////////////////////////////////////

	if ( isSet($_POST['cropx']) ){
		$filename = $_POST['filename'];
		$id = $_POST['imageid'];
		$tag = $_POST['tag'];
		$newsimageshd = isSet($_POST['vizhdswitch']);
		$touchscreen = isSet($_POST['touchscreenswitch']);
		$hln = isSet($_POST['hlnswitch']);
        $jpegDownload = isSet($_POST['jpegswitch']);
		$cropx = floatval($_POST['cropx']);
		$cropy = floatval($_POST['cropy']);
		$cropw = floatval($_POST['cropw']);
		$verticalRight = isSet($_POST['verticalRight']);

		$finalImage = resize($GLOBALS['TMP_PATH'], $filename, $cropx, $cropy, $cropw, $verticalRight);

		////////////////// IF NECESSARY, SEND TO VARIOUS PLACES ////////////////
		if ($touchscreen) {
			$finalImage -> writeImage( 	$GLOBALS['IMAGE_PATH'] . $filename );
		}
		if ($newsimageshd) {
			$finalImage -> writeImage( 	$GLOBALS['NEWSIMAGES_HD_PATH'] . $filename );
		}
		if ($hln) {
			$finalImage -> writeImage( 	$GLOBALS['HLN_PATH'] . $filename );
		}
        if ($jpegDownload){
            $jpegFilename = $tag . $id . '.jpg';
            $finalImage -> writeImage( $GLOBALS['TMP_PATH'] . $jpegFilename );
        }


		sendHeader();
		?>
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>All done!</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				The filename is <b><?php echo $filename;?></b>
			</div>
		</div>
		<?php

		if ($touchscreen) {
			echo '<div class="row">';
				echo "<div class='col-xs-12 text-center'>You can now access it in the \"CNN Images\" area of CNNI Touchscreen templates.</div>\n";
			echo '</div>';
			echo '<div class="row">';
				echo "<div class='col-xs-12 text-center'>";
					echo "<img src=" . $GLOBALS['IMAGE_URL'] . $filename . " width=600>";
				echo "</div>";
			echo '</div>';

		}
		if ($newsimageshd) {
			echo '<div class="row">';
				echo "<div class='col-xs-12 text-center'>It should be available in Viz Newsimages HD within 5 minutes.</div>\n";
			echo '</div>';
		}
		if ($hln) {
			echo '<div class="row">';
				echo "<div class='col-xs-12 text-center'>It should be available in HLN's system within 5 minutes.</div>\n";
			echo '</div>';
		}
        if ($jpegDownload) {
			echo '<div class="row">';
				echo "<div class='col-xs-12 text-center'>You can download the jpeg image <a href='" . $GLOBALS['TMP_URL'] . $jpegFilename . "'>here</a>.</div>\n";
			echo '</div>';
		}
		sendFooter();

		unlink($GLOBALS['TMP_PATH'] . $filename);

		doLog($filename);
        deleteOldTempFiles();
	}

	////////////////////////////////////////////////////////////////
	//////////// FUNCTION TO CREATE NEW API SESSION ////////////////
	////////////////////////////////////////////////////////////////
	function createNewSession($creds) {
		$authEndpoint = "https://api.gettyimages.com/oauth2/token";

		$curl = getCurlForFormPost($authEndpoint);
		setFormData($curl, array("grant_type" => "client_credentials", "client_id" => $creds->system_id, "client_secret" => $creds->system_password));
		$resultObj = executeCurl($curl);
		$result = json_decode($resultObj['body'],true);

		// evaluate for success response
		if ($result === false) {
			sendHeader();
			echo "<p>Sorry.  Something went wrong.  I feel really terrible about that.</p>";
			echo "<p style='margin-top:20px'><a href='/utilities/getty_importer/getty_importer.php'>Go Back</a></p>";
			sendFooter();
			exit;
		}
		$ret = false;
		if ($result) {
	          $ret = new stdClass;
			  $ret->access_token = $result["access_token"];
			  $ret->token_type = $result["token_type"];
	          $ret->expires = date("U") + intval($result["expires_in"]);
		} else {
			sendHeader();
			echo "<p>Sorry.  Something went wrong.  I feel really terrible about that.</p>";
			echo "<p style='margin-top:20px'><a href='/utilities/getty_importer/getty_importer.php'>Go Back</a></p>";
			sendFooter();
			exit;
		}
		//Save our session info to file so it can be reused for 30 minutes.
		file_put_contents($GLOBALS['SESSION_FILE'], json_encode($ret));

		return $ret;
	}

	////////////////////////////////////////////////////////////////
	////////////// FUNCTION TO GET IMAGE INFO //////////////////////
	////////////////////////////////////////////////////////////////
	function getImageInfo($creds, $imageId) {
		$metadataEndpoint = "https://api.gettyimages.com/v3/images?ids=";
		$curl = getCurl($metadataEndpoint . $imageId);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Api-Key:".$creds->system_id));
		$result = json_decode(executeCurl($curl)['body']);

		// evaluate for success response
		$ret = false;
		if ($result && count($result->images) > 0) {
        	$ret = $result->images[0];
		}
		return $ret;
	}

	////////////////////////////////////////////////////////////////
	///////// FUNCTION TO RETRIEVE A DOWNLOAD URL //////////////////
	////////////////////////////////////////////////////////////////
	function getDownloadURL($creds, $tokenType, $token, $imageId) {
		$downloadEndpoint = "https://api.gettyimages.com/v3/downloads/images/" . $imageId . "?auto_download=false";

		$headersToSend = array(CURLOPT_HTTPHEADER => array(
								"Api-Key: " . $creds->system_id,
								"Authorization: " . $tokenType . " " . $token),
								CURLOPT_FOLLOWLOCATION => TRUE);
		$curl = getCurlForPost($downloadEndpoint, $headersToSend);
		$rawResponse = executeCurl($curl);
		$response = json_decode($rawResponse["body"]);
		// evaluate for success response
		if ( $response && isSet($response->uri) ) {
			return $response->uri;	
		} else {
			echo "Sorry.  Something went wrong.  I feel really terrible about that.";
			exit;
		}
	}

	////////////////////////////////////////////////////////////////
	///////////// FUNCTION DOWNLOAD A GETTY IMAGE //////////////////
	////////////////////////////////////////////////////////////////
	function fetchImage($url, $id, $tag, $imageInfo) {
		$title = sanitize( $imageInfo->caption );
		$title = substr( $title, 0, 25);
		$tag = substr( $tag, 0, 25 );
		$filename = $tag . $title . "_" . $id . $GLOBALS['FILENAME_TAG'];

		file_put_contents($GLOBALS['TMP_PATH'] . $tag . $title . "_" . $id . $GLOBALS['FILENAME_TAG'], file_get_contents($url));
		return $filename;
	}

	////////////////////////////////////////////////////////////////
	/////////////// FUNCTION RESIZE A GETTY IMAGE //////////////////
	////////////////////////////////////////////////////////////////
	function resize($imagepath, $filename, $cropxPct, $cropyPct, $cropwPct, $verticalRight) {
	    $image = new Imagick( $imagepath . $filename );
	    $imageprops = $image->getImageGeometry();
	    $origWidth = $imageprops['width'];
	    $origHeight = $imageprops['height'];

		$cropx = $origWidth * $cropxPct;
		$cropy = $origHeight * $cropyPct;
		$cropw = $origWidth * $cropwPct;

		if ($verticalRight) {
			$croph = $cropw * $GLOBALS['HD_ASPECT'];
			$finalWidth = $GLOBALS['HD_HEIGHT'];
			$finalHeight = $GLOBALS['HD_WIDTH'];
		} else {
			$croph = $cropw / $GLOBALS['HD_ASPECT'];
			$finalWidth = $GLOBALS['HD_WIDTH'];
			$finalHeight = $GLOBALS['HD_HEIGHT'];

		}

		$image->cropImage( $cropw, $croph, $cropx, $cropy );
		$image->resizeImage($finalWidth,$finalHeight, Imagick::FILTER_LANCZOS, 0.9);

		if ($verticalRight){
			$image->rotateImage( 'black', 90 );
		}

		return $image;
	}

	////////////////////////////////////////////////////////////////
	////////////////// FUNCTION TO LOG IMPORT //////////////////////
	////////////////////////////////////////////////////////////////
	function doLog( $filename ) {
		$myFile = "/home/cnnitouch/www/cake/touch.log";
		$fh = fopen($myFile, 'a');
		$stringData = 'Getty Image Import ' . $filename . ' ' . date("m.d.y") . "\n";
		fwrite($fh, $stringData);
		fclose($fh);
	}



	////////////////////////////////////////////////////////////////
	/////////////// FUNCTION TO SANITIZE FILENAMES /////////////////
	////////////////////////////////////////////////////////////////
	function sanitize($string, $force_lowercase = false, $anal = false) {
	    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "=", "+", "[", "{", "]",
	                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
	                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
	    $clean = trim(str_replace($strip, "", strip_tags($string)));
	    $clean = preg_replace('/\s+/', "-", $clean);
	    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
	    return ($force_lowercase) ?
	        (function_exists('mb_strtolower')) ?
	            mb_strtolower($clean, 'UTF-8') :
	            strtolower($clean) :
	        $clean;
	}

	////////////////////////////////////////////////////////////////
	////// FUNCTIONS TO SEND HTML HEADER AND FOOTER ////////////////
	////////////////////////////////////////////////////////////////
	function sendHeader() {
		require("/home/cnnitouch/www/adminui/nav.php");
		echo file_get_contents("header.html");

	}
	function sendFooter() {
		echo file_get_contents("footer.html");
	}

    function deleteOldTempFiles(){
        $files = glob($GLOBALS['TMP_PATH']."*");
        $now   = time();

        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * 60 * 24) {
                    unlink($file);
                }
            }
        }

    }


























	/////////////////////////////////////////////////////////////////
	/////// FUNCTIONS TO ADD BLUR TO ACHIEVE SET DIMENSIONS /////////
	/////////////////////////////////////////////////////////////////
	function prepAspect( $im, $newWidth, $newHeight, $minAspect, $maxAspect, $blurAmount ) {
		$size = $im -> getImageGeometry();
		$width = $size['width'];
		$height = $size['height'];
		$aspect = $width / $height;
		if ( $aspect < $minAspect ) {
			$im = handleVertical( $im, $newWidth, $newHeight, $blurAmount );
		} else if ($aspect > $maxAspect) {
			$im = handleHorizontal( $im, $newWidth, $newHeight, $blurAmount );
		}
		return $im;
	}
	function handleVertical($im, $maxWidth, $maxHeight, $blurAmount) {
		$size = $im->getImageGeometry();
		$width = $size['width'];
		$height = $size['height'];

		$shadow = clone $im;
		$shadow->setImageBackgroundColor( new ImagickPixel( 'black' ) );
		$shadow->shadowImage( 80, 20, 0, 0 );
		$shadow->compositeImage( $im, Imagick::COMPOSITE_OVER, 0, 0 );

		$blur = clone $im;
		$blur -> scaleImage( $maxWidth, $maxHeight );
		$blur -> blurImage($blurAmount, $blurAmount);
		$blur -> colorizeImage('#333333', 100);

		$blur->compositeImage( $shadow, Imagick::COMPOSITE_OVER, $maxWidth/2 - $width/2, 0 );
		return $blur;
	}

	function handleHorizontal( $im, $maxWidth, $maxHeight, $blurAmount ) {
		$size = $im->getImageGeometry();
		$width = $size['width'];
		$height = $size['height'];

		$blur = clone $im;
		$blur -> blurImage($blurAmount, $blurAmount);
		$blur -> scaleImage( $maxWidth, $maxHeight );

		$shadow = clone $im;
		$shadow->setImageBackgroundColor( new ImagickPixel( 'black' ) );
		$shadow->shadowImage( 80, 20, 0, 0 );
		$shadow->compositeImage( $im, Imagick::COMPOSITE_OVER, 0, 0 );

		$verticalLetterboxOffset = ($maxHeight - $height) * .2;

		$blur->compositeImage( $shadow, Imagick::COMPOSITE_OVER, 0, $maxHeight/2 - $height/2 - $verticalLetterboxOffset);
		return $blur;
	}
