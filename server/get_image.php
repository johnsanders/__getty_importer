<?php 
	require("./GettyFunctions.php");
	require("./GettyImportFunctions.php");

	$CREDENTIALS_FILE = "creds.json";
	$SESSION_FILE = "session_v3.json";
	$FILENAME_TAG = "_GETTY.jpg";
	$TMP_PATH = "/home/cnnitouch/www/utilities/getty_importer/tmp/";
	if (!isSet($_GET["id"]) || $_GET["id"] === "") {
		echo json_encode( ["status" => "error", "message" => "Need an image ID, pleez"] );
		exit;
	} else {
		$id = $_GET["id"];
	}
	if(isSet($_GET["tag"])) {
		$tag = $_GET["tag"];
	} else {
		$tag = "";
	}
	////////////////////////////////////////////////////////////////
	/////////////////////  GET API CREDENTIALS  ////////////////////
	////////////////////////////////////////////////////////////////
	$creds = json_decode( file_get_contents($CREDENTIALS_FILE) );
	////////////////////////////////////////////////////////////////
	/////// CHECK IF WE STILL HAVE AN ACTIVE API SESSION ///////////
	////////////////////////////////////////////////////////////////
	$session = json_decode( file_get_contents($SESSION_FILE) );
	$sessionExpires = (int)$session->expires;
	$now = (int)date("U");
	if ( $now >  $sessionExpires ) {
		$token = createNewSession($creds, $SESSION_FILE);
	} else {
		$token = $session;
	}

	////////////////////////////////////////////////////////////////
	////////////// GET INFO ABOUT IMAGE  ///////////////////////////
	////////////// GET DOWNLOAD URL  ///////////////////////////////
	////////////// USE URL TO FETCH IMAGE  /////////////////////////
	////////////////////////////////////////////////////////////////
	$imageInfo = getImageInfo($creds, $token, $id);
	$downloadUrl = getDownloadURL($creds, $token->token_type, $token->access_token, $id);
	$title = sanitize( $imageInfo->title );
	$width = $imageInfo->max_dimensions->width;
	$height = $imageInfo->max_dimensions->height;
	$title = substr( $title, 0, 25);
	if (file_exists( $TMP_PATH . $tag . $title . "_" . $id . $FILENAME_TAG )){
		$filename = $tag . $title . "_" . $id . $FILENAME_TAG;
	} else {
		$filename = fetchImage($downloadUrl, $id, $tag, $imageInfo, $FILENAME_TAG, $TMP_PATH);
	}
	echo json_encode( ["status" => "ok", "filename" => $filename, "width" => $width, "height" => $height] );
	exec( "find ${TMP_PATH}*.jpg -mmin +$((60*24)) -exec rm {} \;"   );
	exit;
?>
