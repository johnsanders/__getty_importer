<?php
	$destinations = [
		"newsimage" => "/mnt/HD_IMAGES_WRITABLE/",
		"touchscreen" => "/home/cnnitouch/www/cnnimages/",
		"informed" => "/mnt/HD_STAYING_INFORMED_WRITABLE/",
		"bigWallTease" => "", //ARKIV\new_arkiv\CNNT_WALL_TEASE
		"vistaFull" => "/mnt/VISTA_FULL_WRITABLE/",
		"vistaBackground" => "/mnt/VISTA_BG_WRITABLE/",
		"talkHorizontal" => "/home/cnnitouch/www/apps/misc/cnn_talk/swirl/swirlDisplay/img/sites/"
	];
	$TMP_PATH = "/home/cnnitouch/www/utilities/getty_importer/tmp/";

	$filename = $_POST['filename'];
	$cropx = floatval($_POST['cropX']);
	$cropy = floatval($_POST['cropY']);
	$scale = floatval($_POST['scale']);
	$imageType = $_POST['imageType'];

	$finalImage = resize($TMP_PATH, $filename, $cropx, $cropy, $scale, $imageType);
	////////////////// IF NECESSARY, SEND TO DESTINATION////////////////
	if (isSet($destinations[$imageType])) {
		$finalImage -> writeImage( $destinations[$imageType] . $filename )	;
	}
	$finalImage -> writeImage( $TMP_PATH . "crop_" . $filename );
	exit;

	////////////////////////////////////////////////////////////////
	/////////////// FUNCTION RESIZE A GETTY IMAGE //////////////////
	////////////////////////////////////////////////////////////////
	function resize($imagepath, $filename, $cropxPct, $cropyPct, $scale, $imageType) {
		$HD_WIDTH = 1920;
		$HD_HEIGHT = 1080;
		$HD_ASPECT = $HD_WIDTH / $HD_HEIGHT;
		$INFORMED_WIDTH = 884;
		$INFORMED_HEIGHT = 1080;
		$INFORMED_ASPECT = $INFORMED_WIDTH / $INFORMED_HEIGHT;
		$BIG_WALL_WIDTH = 1920;
		$BIG_WALL_HEIGHT = 540;
		$BIG_WALL_ASPECT = $BIG_WALL_WIDTH / $BIG_WALL_HEIGHT;
		$image = new Imagick( $imagepath . $filename );
		$imageprops = $image->getImageGeometry();
		$origWidth = $imageprops['width'];
		$origHeight = $imageprops['height'];
		$cropx = $origWidth * $cropxPct;
		$cropy = $origHeight * $cropyPct;
		if ($imageType == "verticalRight") {
			$croph = $origHeight / $scale;
			$cropw = $croph / $HD_ASPECT;
			$finalWidth = $HD_HEIGHT;
			$finalHeight = $HD_WIDTH;
		} else if ($imageType == "newsimage" || $imageType === "touchscreen" || $imageType === "talkHorizontal"){
			$cropw = $origWidth / $scale;
			$croph = $cropw / $HD_ASPECT;
			$finalWidth = $HD_WIDTH;
			$finalHeight = $HD_HEIGHT;
		} else if ($imageType == "informed") {
			$croph = $origHeight / $scale;
			$cropw = $croph * $INFORMED_ASPECT;
			$finalWidth = $INFORMED_WIDTH;
			$finalHeight = $INFORMED_HEIGHT;
		} else if ($imageType === "bigWallImage" || $imageType === "vistaBackground" || $imageType === "vistaFull") {
			$cropw = $origWidth / $scale;
			$croph = $cropw / $BIG_WALL_ASPECT;
			$finalWidth = $BIG_WALL_WIDTH;
			$finalHeight = $BIG_WALL_HEIGHT;
		}
		$image->cropImage( $cropw, $croph, $cropx, $cropy );
		$image->resizeImage($finalWidth,$finalHeight, Imagick::FILTER_LANCZOS, 0.9);

		if ($imageType == "verticalRight"){
			$image->rotateImage( 'black', 90 );
		}
		return $image;
	}
	////////////////////////////////////////////////////////////////
	////////////////// FUNCTION TO LOG IMPORT //////////////////////
	////////////////////////////////////////////////////////////////
	function doLog( $filename ) {
		$myFile = "/home/cnnitouch/www/cake/touch.log";
		$fh = fopen($myFile, 'a');
		$stringData = 'Getty Image Import ' . $filename . ' ' . date("m.d.y") . "\n";
		fwrite($fh, $stringData);
		fclose($fh);
	}
?>
