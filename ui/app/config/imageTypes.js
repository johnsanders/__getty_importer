export default {
	newsimage:{
		name:"newsimage",
		label:"Viz / Newsimages",
		description:"A normal HD news image",
		aspect:1920/1080,
		overlay:true,
		guide:true
	},
	touchscreen:{
		name:"touchscreen",
		label:"Touchscreen",
		description:"A normal touchscreen image",
		aspect:1920/1080,
		overlay:true,
		guide:true
	},
	/*
	informed:{
		name:"informed",
		label:"Staying Informed",
		description:"Specific format for 'Staying Informed' pilot",
		aspect:884/1080,
		overlay:false,
		guide:false
	},
	*/
	verticalRight:{
		name:"verticalRight",
		label:"Vertical (Right)",
		description:"A 'sideways' image for use in vertical screens",
		aspect:1080/1920,
		overlay:false,
		guide:false
	},
	bigWallTease:{
		name:"bigWallTease",
		label:"Big Wall Tease",
		description:"Specific format for Studio 7 tease pilot",
		aspect:1920/1080,
		overlay:true,
		guide:true
	},
	vistaFull:{
		name:"vistaFull",
		label:"Vista Full",
		description:"Studio 7 Vista wall full image",
		aspect:1920/540,
		overlay:true,
		guide:true
	},
	vistaBackground:{
		name:"vistaBackground",
		label:"Vista Background",
		description:"Studio 7 Vista wall text safe background",
		aspect:1920/540,
		overlay:true,
		guide:true
	},
	talkHorizontal: {
		name:"talkHorizontal",
		label:"CNN Talk Horizontal",
		description:"Special format for CNN Talk",
		aspect:1920/1080,
		overlay:false,
		guide:true
	}
};
