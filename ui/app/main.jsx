import React from "react";
import {render} from "react-dom";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import "./styles/style.css";
import GettyContainer from "./components/GettyContainer";
import UploadContainer from "./components/UploadContainer";
import EditContainer from "./components/EditContainer";
import SearchContainer from "./components/SearchContainer";

render((
	<BrowserRouter basename="/utilities/getty_importer/ui">
		<Switch>
			<Route exact path="*/find" component={GettyContainer} />
			<Route exact path="*/upload" component={UploadContainer} />
			<Route exact path="*/search" component={SearchContainer} />
			<Route
				path="*/edit/:filename/:width/:height"
				exact
				render={ (props) =>
					<EditContainer {...props} />
				}
			/>
		</Switch>
	</BrowserRouter>
), document.getElementById("app"));
