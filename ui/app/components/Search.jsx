import React from "react";
import SearchInput from "./SearchInput";
import SearchResult from "./SearchResult";
import SearchResultDetail from "./SearchResultDetail";
import SearchFavoritePanel from "./SearchFavoritePanel";
import Pagination from "rc-pagination";
import LoadingSpinner from "./LoadingSpinner";
import Notification from "./Notification";
import "rc-pagination/assets/index.css";

const Search = props => (
	<div style={{position:"relative", top:"75px"}}>
		<SearchInput
			searchTerm={props.searchTerm}
			loading={props.loading}
			handleSearchTermChange={props.handleSearchTermChange}
			handleSearchClick={props.handleSearchClick}
		/>
		<div className="resultsGrid">
			{
				props.results.map(result => (
					<SearchResult
						key={result.id}
						result={result}
						isFavorite={props.favorites.includes(result)}
						toggleFavorite={props.toggleFavorite}
						handleMouseOver={props.handleMouseOver}
						setCropId={props.setCropId}
					/>
				))
			}
		</div>
		{
			props.detailId === "" ? null :
				<SearchResultDetail
					detailId={props.detailId}
					hoveredResultRect={props.hoveredResultRect}
					results={props.results}
				/>
		}
		{
			props.totalResults === 0 ? null :
				<div className="text-center mb-5">
					<Pagination
						onChange={props.handlePaginationChange}
						current={props.currentPagination}
						total={props.totalResults}
						pageSize={props.resultsPerPage}
						hideOnSinglePage={true}
					/>
					<LoadingSpinner
						top="-8px"
						left="20px"
						visible={props.loading}
					/>
				</div>
		}
		<SearchFavoritePanel
			favorites={props.favorites}
			updateFavorites={props.updateFavorites}
		/>
		<Notification
			message={props.notificationText}
			className={props.notificationStyle}
			style={{position:"absolute", top:"60px", right:"20px"}}
		/>
	</div>
);
export default Search;
