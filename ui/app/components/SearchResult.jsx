import React from "react";
import dateformat from "dateformat";
import SearchResultImage from "./SearchResultImage";
import SearchResultButtons from "./SearchResultButtons";

const titleMaxLength = 80;

class SearchResult extends React.Component {
	constructor(props) {
		super(props);
	}
	render(){
		const {result} = this.props;
		const date = new Date(result.date_created);
		const dateString = dateformat(date, "mmm. d, yyyy");
		const titleRaw = result.title;
		const title = titleRaw.length < titleMaxLength ? titleRaw : titleRaw.substring(0, titleMaxLength) + "...";
		return (
			<div className="card m-1" style={{width:"200px"}}>
				<div className="card-body">
					<SearchResultImage
						handleMouseOver={this.props.handleMouseOver}
						result={this.props.result}
					/>
					<p className="card-title">{title}</p>
				</div>
				<div style={{display:"flex"}}>
					<p className="two-items card-body">
						<span className="text-muted">
							{dateString}
						</span>
						<span className="text-muted">
							{result.id}
						</span>
					</p>
				</div>
				<SearchResultButtons
					result={result}
					setCropId={this.props.setCropId}
					toggleFavorite={this.props.toggleFavorite}
					isFavorite={this.props.isFavorite}
				/>
			</div>
		);
	};
};

export default SearchResult;
