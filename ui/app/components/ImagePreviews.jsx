import React from "react";
import ImagePreview from "./ImagePreview";

const ImagePreviews = props => {
	return (
		<div className="form-group">
			<div className="col-xs-12 text-center">
				{props.acceptedFiles.map((file, index) => (
					<ImagePreview 
						key={"img_" + index.toString()}
						src={file.preview} 
					/> 
				))}
			</div>
		</div>
	);
};

export default ImagePreviews;
