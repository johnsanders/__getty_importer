import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import Upload from "./Upload";
import PropTypes from "prop-types";

class UploadContainer extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.state = {
			loading:false,
			notificationText:"",
			acceptedFiles:[]
		};
	}
	clearNotification(delay) {
		setTimeout(() => this.setState({notificationText:""}), delay * 1000);
	}
	onChange(e) {
		this.setState({[e.target.getAttribute("name")]: e.target.value});
	}
	onDrop( accepted, rejected ){
		console.log(accepted, rejected);
		this.setState( {acceptedFiles:this.state.acceptedFiles.concat(accepted)} );
	}
	doUpload(e) {
		e.preventDefault();
		const data = new FormData();
		this.state.acceptedFiles.forEach( file => { 
			data.append( file.name, file );
			data.append( file.name + "_data", {name:file.name, destinations:[], crop:{x:0, y:0, w:1, h:1}} );
		});
		const axiosConfig = {
			onUploadProgress: progressEvent => {
				console.log(progressEvent.loaded*100 / progressEvent.total);
			}
		};
		axios.post("/apps/imageImporter/handleUpload.php", data, axiosConfig)
			.then( response => {
				console.log(response.data);
			}).catch ( err => console.error(err) );
	}
	render() {
		return(
			<div>
				<Upload
					onChange={this.onChange}
					onDrop={this.onDrop}
					doUpload={this.doUpload}
					loading={this.state.loading}
					notificationText={this.state.notificationText}
					acceptedFiles={this.state.acceptedFiles}
				/>
			</div>
		);
	}
}
UploadContainer.propTypes = {
	history:PropTypes.object.isRequired
};
export default UploadContainer;


