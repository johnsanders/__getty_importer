import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import ReactTooltip from "react-tooltip";
import Nav from "@johnsand/nav";
import Search from "./Search";
import SearchCropperWindowContainer from "./SearchCropperWindowContainer";

const apiUrl = "/utilities/getty_importer/getty_importer_v3.php";
const resultsPerPage = 50;

class SearchContainer extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.state = {
			searchTerm:"supreme court",
			loading:false,
			results:[],
			favorites:[],
			totalResults:0,
			currentPagination:1,
			detailId:"",
			cropId:"",
			notificationText:"",
			notificationStyle:"alert-success"
		};
	}
	componentDidUpdate() {
		ReactTooltip.rebuild();
	}
	setNotification(msg, style) {
		this.setState({notificationText:msg, notificationStyle:style});
		this.clearNotification(3);
	}
	clearNotification(secs) {
		setTimeout(() => this.setState({notificationText:""}), secs * 1000);
	}
	handleNewSearch(e) {
		e.preventDefault();
		this.setState({currentPagination:1}, () => this.handleSearch(true));
		this.handleSearch(true);
	}
	handleSearch(newSearch) {
		if (this.state.loading || this.state.searchTerm.length < 3) {
			return;
		}
		this.setState({loading:true});
		const params = {
			phrase:this.state.searchTerm,
			page:this.state.currentPagination.toString()
		};
		axios.get(apiUrl, {params}).then(res => {
			const results = res.data.images.slice(0).map(img => {
				img.cnn_preview = img.display_sizes.find(size => size.name === "preview").uri;
				img.cnn_comp = img.display_sizes.find(size => size.name === "comp").uri;
				img.cnn_thumb = img.display_sizes.find(size => size.name === "thumb").uri;
				return img;
			});
			this.setState({
				results:results,
				totalResults:res.data.result_count,
				loading:false
			});
			if (!newSearch) {
				window.scroll({top:0, behavior:"smooth"});
			}
		}).catch(e => console.error(e));
		return false;
	}
	handleSearchTermChange(e) {
		this.setState({searchTerm:e.target.value});
	}
	handleMouseOver(e) {
		if (e.type === "mouseover") {
			const id = e.currentTarget.getAttribute("data-id");
			const rect = e.currentTarget.getBoundingClientRect();
			this.hoverTimeout = window.setTimeout(() => {
				this.setState({detailId:id, hoveredResultRect:rect});
			}, 500);
		} else {
			window.clearTimeout(this.hoverTimeout);
			this.setState({detailId:""});
		}
	}
	setCropId(e) {
		if (typeof e === "string") {
			this.setState({cropId:e});
		} else {
			this.setState({cropId:e.currentTarget.getAttribute("data-id")});
		}
	}
	toggleFavorite(e) {
		const id = e.currentTarget.getAttribute("data-id");
		const fav = this.state.results.find(result => result.id === id);
		if (this.state.favorites.includes(fav)) {
			this.setState({favorites:this.state.favorites.filter(f => f.id !== id)});
		} else {
			this.setState({favorites:this.state.favorites.concat([fav])});
		}
	}
	updateFavorites(favorites) {
		this.setState({favorites});
	}
	handlePaginationChange(page) {
		this.setState({currentPagination:page}, () => this.handleSearch(false));
	}
	render() {
		const cropResult = this.state.results.find(result => result.id === this.state.cropId);
		return (
			<div>
				<ReactTooltip />
				<Nav />
				<Search
					results={this.state.results}
					favorites={this.state.favorites}
					searchTerm={this.state.searchTerm}
					loading={this.state.loading}
					handleSearchTermChange={this.handleSearchTermChange}
					handleSearchClick={this.handleNewSearch}
					handleMouseOver={this.handleMouseOver}
					setCropId={this.setCropId}
					toggleFavorite={this.toggleFavorite}
					updateFavorites={this.updateFavorites}
					currentPagination={this.state.currentPagination}
					totalResults={this.state.totalResults}
					handlePaginationChange={this.handlePaginationChange}
					resultsPerPage={resultsPerPage}
					detailId={this.state.detailId}
					hoveredResultRect={this.state.hoveredResultRect}
					notificationText={this.state.notificationText}
					notificationStyle={this.state.notificationStyle}
				/>
				{
					cropResult === undefined ? null :
						<SearchCropperWindowContainer
							cropId={this.state.cropId}
							setCropId={this.setCropId}
							results={this.state.results}
							onCropUpdate={this.onCropUpdate}
							setNotification={this.setNotification}
						/>
				}
			</div>
		);
	}
}

export default SearchContainer;
