import React from "react";
import Nav from "@johnsand/nav";
import Dropzone from "react-dropzone";
import Notification from "./Notification";
import ImagePreviews from "./ImagePreviews";
import PropTypes from "prop-types";

const Upload = props => (
	<form className="form-horizontal">
		<Nav />
		<div className="page-header" style={{marginTop:"80px"}}>
			<h1 className="text-center text-uppercase">IMAGE UPLOADER</h1>
		</div>
		<div className="form-group">
			<div className="col-xs-12">
				<Dropzone 
					className="dropZone"
					acceptClassName="dropZoneAccept"
					rejectClassName="dropZoneReject"
					accept="image/jpeg, image/png"	
					onDrop={props.onDrop}
				>
					<div className="well">
						Drop files here, or click here to choose from your computer
					</div>
				</Dropzone>
			</div>
		</div>
		<ImagePreviews acceptedFiles={props.acceptedFiles} />
		<div className="col-xs-12 text-center">
			<button onClick={props.doUpload} className="btn btn-primary" style={{width:"100px"}}>
				Upload
			</button>
		</div>
		<div className="form-group">
			<div className="col-xs-1">
				<div
					fadeIn="none"
					name="double-bounce"
					style={{display:props.loading ? "" : "none"}}
				/>
			</div>
			<div className="col-xs-6">
				<Notification
					message={props.notificationText}
				/>
			</div>
		</div>
	</form>
);
Upload.propTypes = {
	onChange:PropTypes.func.isRequired,
	loading:PropTypes.bool.isRequired,
	notificationText:PropTypes.string.isRequired
};
export default Upload;
