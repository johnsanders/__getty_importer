import React from "react";

const SearchResultDetail = props => {
	const imageWidth = 400;
	const pad = 10;
	const sampleCharCount = 500;
	const sampleTextHeight = 325;
	const heightPerChar = sampleTextHeight / sampleCharCount;

	const result = props.results.find( result => result.id === props.detailId );
	const uri = result.display_sizes.find( size => size.name==="preview" ).uri;
	const aspect = result.max_dimensions.height / result.max_dimensions.width;
	const imageHeight = imageWidth * aspect;
	const estTextHeight = result.caption.length * heightPerChar;
	const resultX = props.hoveredResultRect.left + window.pageXOffset;
	const resultY = props.hoveredResultRect.top + window.pageYOffset - 50;
	const resultWidth = props.hoveredResultRect.width;
	const resultHeight = props.hoveredResultRect.height;
	const posX = resultX < window.innerWidth -resultWidth - imageWidth - pad
		? resultX + props.hoveredResultRect.width + pad
		: resultX - imageWidth - pad * 3;
	const posY = resultY < window.innerHeight - imageHeight - estTextHeight - pad
		? resultY
		: window.innerHeight + window.pageYOffset - imageHeight - estTextHeight - pad * 2;

	return (
		<div
			className="searchResultDetail"
			style={{
				width:(imageWidth + pad * 2).toString() + "px",
				padding:pad.toString() + "px",
				left:posX.toString() + "px",
				top:posY.toString() + "px",
				opacity:props.detailId === "" ? 0 : 1
			}}
		>
			<img
				src={uri}
				style={{
					width:imageWidth.toString() + "px",
					height:imageHeight.toString() + "px"
				}}
			/>
			<div className="detailCaption">
				{result.caption}
			</div>
		</div>
	);
};

export default SearchResultDetail;
