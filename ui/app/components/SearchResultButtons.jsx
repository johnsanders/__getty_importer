import React from "react";
import autobind from "react-autobind";
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/fontawesome-pro-solid";

class SearchResultButtons extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.state={dropdownVisible:false};
	}
	handleDropdownToggle() {
		this.setState({dropdownVisible:!this.state.dropdownVisible});
	}
	render() {
		return (
			<div
				className="card-footer"
				style={{display:"flex", justifyContent:"center"}}
			>
				<div className="btn-group">
					<button
						className="btn btn-xs btn-secondary"
						data-id={this.props.result.id}
						onClick={this.props.setCropId}
					>
						Send to CNN
					</button>
					<button
						data-id={this.props.result.id}
						data-tip="Favorite"
						className={"btn btn-xs " + (this.props.isFavorite
							? "btn-warning active" : "btn-secondary")}
						onClick={this.props.toggleFavorite}
					>
						<Icon icon={faStar} />
					</button>
					<button
						onClick={this.handleDropdownToggle}
						data-id={this.props.result.id}
						className="btn btn-xs btn-secondary"
					>
						More <span className="small">&#x25BC;</span>
					</button>
				</div>
				{
					this.state.dropdownVisible
						?
						<div className="dropdown-menu">
							<a className="dropdown-item">Thing</a>
							<a className="dropdown-item">Other Thing</a>
						</div>
						: false
				}
			</div>
		);
	}
}

export default SearchResultButtons;
