import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import qs from "qs";
import filenamify from "filenamify";
import Getty from "./Getty";
import PropTypes from "prop-types";

class GettyContainer extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		const {extension, imageId} = qs.parse(this.props.location.search, {ignoreQueryPrefix:true});
		this.isExtension = extension === "1";
		this.state = {
			loading:false,
			imageId:imageId || "",
			imageTag:"",
			notificationText:""
		};
	}
	clearNotification(delay) {
		setTimeout(() => this.setState({notificationText:""}), delay * 1000);
	}
	getImage(e) {
		e.preventDefault();
		if (this.state.loading) { return; }
		this.setState({loading:true});
		const tag = filenamify(this.state.imageTag).replace(" ", "_");
		const url = "/utilities/getty_importer/get_image.php?id="
			+ this.state.imageId + "&tag=" + tag;
		axios.get(url).then(response => {
				this.setState({loading:false});
				if (response.data.status && response.data.status === "ok") {
					const filename = response.data.filename;
					const width = response.data.width;
					const height = response.data.height;
					const editUrl = `/edit/${filename}/${width}/${height}` +
						this.props.location.search;
					this.props.history.push(editUrl);
				} else {
					this.setState({notificationText:response.data.message});
					this.clearNotification(3);
				}
			});
	}
	onChange(e) {
		this.setState({[e.target.getAttribute("name")]: e.target.value});
	}
	render() {
		return(
			<div className={this.isExtension ? "extensionContainer" : null}>
				<Getty
					getImage={this.getImage}
					imageId={this.state.imageId}
					imageTag={this.state.imageTag}
					onChange={this.onChange}
					loading={this.state.loading}
					notificationText={this.state.notificationText}
					isExtension={this.isExtension}
				/>
			</div>
		);
	}
}
GettyContainer.propTypes = {
	history:PropTypes.object.isRequired
};
export default GettyContainer;


