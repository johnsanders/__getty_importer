import React from "react";
import autobind from "react-autobind";
import PropTypes from "prop-types";
import Cropper from "./Cropper";
import imageTypes from "../config/imageTypes";
const tmpPath = "/utilities/getty_importer/tmp/";
const displayWidth = 900;
const maxDisplayHeight = 500;

class CropperContainer extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.state = {};
	}
	componentDidMount() {
		this.setState({showGuide:true});
		this.reset();
	}
	reset() {
		const {width, height} = this.props;
		const newState = this.getResetVals(width, height, this.props.imageType);
		this.setState(newState, this.onUpdate);
	}
	getResetVals(nativeWidth, nativeHeight, imageType) {
		const nativeAspect = parseInt(nativeWidth) / parseInt(nativeHeight);
		const cropAspect = imageTypes[imageType].aspect;
		const crop = this.calculateCrop(cropAspect);
		const imageIsTallerThanCrop = nativeAspect < cropAspect;
		const imageWidth = imageIsTallerThanCrop ? crop.width : crop.height * nativeAspect;
		const imageHeight = imageIsTallerThanCrop ? imageWidth / nativeAspect : crop.height;
		const bounds = this.getBounds(imageWidth, imageHeight, crop, 1);
		const x = -((imageWidth - crop.width) / 2);
		const y = -((imageHeight - crop.height) / 2);
		return {bounds, imageHeight, imageWidth, crop, imageType, scaleValue:1, offset:{x, y}};
	}
	onUpdate() {
		const {imageWidth, imageHeight, offset, scaleValue} = this.state;
		const {imageType} = this.props;
		const x = Math.abs(offset.x / (imageWidth * scaleValue));
		const y = Math.abs(offset.y / (imageHeight * scaleValue));
		this.props.onCropUpdate(x, y, scaleValue, imageType, scaleValue);
	}
	onDragStart() {
		this.setState({showGuide:false});
	}
	onDragStop(e, info) {
		this.setState({offset:{x:info.x, y:info.y}}, this.onUpdate);
	}
	onScaleChange(newScale) {
		const {imageWidth, imageHeight, offset, crop, scaleValue} = this.state;
		const oldBounds = this.getBounds(imageWidth, imageHeight, crop, scaleValue);
		const {left, top} = oldBounds;
		const xOffsetPct = left === 0 ? 0.5 : 1-((left - offset.x) / left);
		const yOffsetPct = top === 0 ? 0.5 : 1-((top - offset.y) / top);
		const unboundX = -(imageWidth * newScale - crop.width) * xOffsetPct;
		const unboundY = -(imageHeight * newScale - crop.height) * yOffsetPct;
		const x = this.limitToBound(unboundX, imageWidth, crop.width, newScale);
		const y = this.limitToBound(unboundY, imageHeight, crop.height, newScale);
		const bounds = this.getBounds(imageWidth, imageHeight, crop, newScale);
		this.setState({scaleValue:newScale, offset:{x,y}, bounds}, this.onUpdate);
	}
	limitToBound(offset, imageSize, crop, scale) {
		const bound = this.getBound(imageSize, crop, scale);
		return offset > bound ? offset : bound;
	}
	calculateCrop(cropAspect) {
		const isHorizontal = cropAspect > 1;
		const width = isHorizontal ? displayWidth : maxDisplayHeight * cropAspect;
		const height = isHorizontal ? displayWidth / cropAspect : maxDisplayHeight;
		return {width, height};
	}
	getBounds(imageWidth, imageHeight, crop, scale) {
		return {
			top:this.getBound(imageHeight, crop.height, scale),
			left:this.getBound(imageWidth, crop.width, scale),
			bottom:0,
			right:0
		};
	}
	getBound(imageSize, cropSize, scale) {
		return -imageSize * scale + cropSize;
	}
	onTypeChange(e) {
		const newType = e.target.value;
		this.props.handleImageTypeChange(newType)
			.then(() => this.reset());
	}
	render() {
		return !this.state.crop ? null : (
			<Cropper
				imageUrl={this.props.filename ? tmpPath + this.props.filename : this.props.imageUrl}
				imageWidth={this.state.imageWidth}
				imageHeight={this.state.imageHeight}
				crop={this.state.crop}
				aspect={this.state.aspect}
				onDragStart={this.onDragStart}
				onDragStop={this.onDragStop}
				offset={this.state.offset}
				imageType={this.props.imageType}
				onTypeChange={this.onTypeChange}
				onScaleChange={this.onScaleChange}
				scaleValue={this.state.scaleValue}
				bounds={this.state.bounds}
				showGuide={this.state.showGuide}
			/>
		);
	}
}
CropperContainer.propTypes = {
	onCropUpdate:PropTypes.func.isRequired,
	handleImageTypeChange:PropTypes.func.isRequired,
	filename:PropTypes.string,
	imageUrl:PropTypes.string.isRequired,
	imageType:PropTypes.string.isRequired,
	width:PropTypes.string.isRequired,
	height:PropTypes.string.isRequired
};
export default CropperContainer;
