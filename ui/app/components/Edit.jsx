import React from "react";
import LoadingSpinner from "./LoadingSpinner";
import CropperContainer from "./CropperContainer";
import PropTypes from "prop-types";

const Edit = props => (
	<div style={{marginBottom:"30px"}}>
		{props.isExtension ? null : 
			<div className="page-header" style={{marginTop:"80px"}}>
				<h1 className="text-center text-uppercase">GETTY IMPORTER</h1>
			</div>
		}
 
		<h4 className="text-center">Choose destination, then adjust so it looks good on TV.</h4>
		{/* VERTICAL WARNING */}
		<div style={{marginTop:"10px"}}>
			<CropperContainer
				onCropUpdate={props.onCropUpdate}
				imageType={props.imageType}
				handleImageTypeChange={props.handleImageTypeChange}
				filename={props.imageFilename}
				width={props.imageWidth}
				height={props.imageHeight}
			/>
		</div>
		<div className="row">
			<div className="col-sm-12 text-center">
				<button className={"btn btn-primary" + (props.loading ? " disabled" : "")} onClick={props.onSubmit}>
					Save Image
				</button>
				<span style={{position:"relative", width:0, height:0}}>
					<LoadingSpinner
						top="0"
						left="10px"
						visible={props.loading}
					/>
				</span>
			</div>
		</div>
	</div>
);
Edit.propTypes = {
	imageFilename:PropTypes.string.isRequired,
	imageWidth:PropTypes.string.isRequired,
	imageHeight:PropTypes.string.isRequired,
	onCropUpdate:PropTypes.func.isRequired,
	loading:PropTypes.bool.isRequired,
	onSubmit:PropTypes.func.isRequired
};
export default Edit;
