import React from "react";
import axios from "axios";
import autobind from "react-autobind";
import filenamify from "filenamify";
import imageTypes from "../config/imageTypes";
import SearchCropperWindow from "./SearchCropperWindow";

const imageWidth = 500;

class SearchCropperWindowContainer extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.state = {imageType:"touchscreen", loading:false, imageTag:""};
	}
	clearNotification(delay) {
		setTimeout(() => this.setState({notificationText:""}), delay * 1000);
	}
	onCropUpdate(x, y, scale, imageType) {
		this.cropX = x;
		this.cropY = y;
		this.scale = scale;
		this.imageType = imageType;
	}
	handleImageTypeChange(newType) {
		return new Promise(resolve => {
			this.setState({imageType:newType}, () => resolve());
		});
	}
	handleCancel() {
		this.props.setCropId("");
	}
	downloadAndCrop(e) {
		e.preventDefault();
		if (this.state.loading) { return; }
		this.setState({loading:true});
		const tag = filenamify(this.state.imageTag).replace(" ", "_");
		const url = "/utilities/getty_importer/get_image.php?id="
			+ this.props.cropId + "&tag=" + tag;
		axios.get(url).then(response => {
			this.setState({cropId:false});
			if (response.data.status && response.data.status === "ok") {
				const filename = response.data.filename;
				this.doCrop(filename, this.cropX, this.cropY, this.scale, this.state.imageType)
					.then(response => {
						///// TODO: NEED TO CHECK FOR FAILURE IN CROP STEP
						this.setState({loading:false});
						this.props.setCropId("");
						const typeName = imageTypes[this.state.imageType].label;
						const msg = "Image imported to " + typeName + ".";
						this.props.setNotification(msg, "alert-success");
					});
			} else {
				console.error(response.data);
				this.props.setNotification(response.data.message);
			}
		});
	}
	doCrop(filename, cropX, cropY, scale, imageType) {
		const params = new URLSearchParams();
		params.append("filename", filename);
		params.append("cropX", cropX);
		params.append("cropY", cropY);
		params.append("scale", scale);
		params.append("imageType", imageType);
		return axios.post("/utilities/getty_importer/edit_image_2.php", params);
	}
	render() {
		const cropResult = this.props.results.find(result => result.id === this.props.cropId);
		const aspect = cropResult.max_dimensions.height / cropResult.max_dimensions.width;
		const imageHeight = imageWidth * aspect;
		return (
			<SearchCropperWindow
				onCropUpdate={this.onCropUpdate}
				imageType={this.state.imageType}
				handleImageTypeChange={this.handleImageTypeChange}
				downloadAndCrop={this.downloadAndCrop}
				loading={this.state.loading}
				imageUrl={cropResult.cnn_preview}
				imageWidth={imageWidth}
				imageHeight={imageHeight}
			/>
		);
	}
}

export default SearchCropperWindowContainer;
