import React from "react";
import Draggable from "react-draggable";
import Slider from "rc-slider";
import ImageTypeSelector from "./ImageTypeSelector";
import Overlays from "./Overlays";
import DragGuide from "./DragGuide";
import "rc-slider/assets/index.css";
import PropTypes from "prop-types";
import imageTypes from "../config/imageTypes";

const Handle = Slider.Handle;
const handle = props => {
	const {value, ...restProps} = props;
	return (
		<Handle value={value} {...restProps} />
	);
};

const Cropper = props => {
	const imageWidth = props.imageWidth * props.scaleValue;
	const imageHeight = props.imageHeight * props.scaleValue;
	const cropAspect = imageTypes[props.imageType].aspect;
	return (
		<div>
			<ImageTypeSelector
				onTypeChange={props.onTypeChange}
				imageType={props.imageType}
			/>
			<div
				id="cropper"
				style={{
					display:"flex",
					justifyContent:"center",
					alignItems:"center",
					marginBottom:"20px"
				}}>
				<Slider
					style={{height:props.crop.height/2, marginRight:"20px"}}
					min={1}
					max={2}
					step={0.1}
					value={props.scaleValue}
					vertical={true}
					dots={true}
					onChange={props.onScaleChange}
					handle={handle}
				/>
				<div
					style={{
						width:props.crop.width,
						height:props.crop.height,
						overflow:"hidden",
						userSelect:"none"
					}}
				>
					{
						props.showGuide && imageTypes[props.imageType].guide ?
							<DragGuide
								crop={props.crop}
								cropAspect={cropAspect}
							/>
							: null
					}
					{
						props.showGuide ? null :
							<Overlays
								imageType={props.imageType}
								crop={props.crop}
								cropAspect={cropAspect}
							/>
					}
					<Draggable
						onStart={props.onDragStart}
						onStop={props.onDragStop}
						position={props.offset}
						bounds={props.bounds}
					>
						<img
							src={props.imageUrl}
							style={{
								userSelect:"none",
								width:imageWidth.toString() + "px",
								height:imageHeight.toString() + "px",
								cursor:"move"
							}}
							ref={el => {if(el) el.draggable = false;}}
						/>
					</Draggable>
				</div>
			</div>
		</div>
	);
};
Cropper.propTypes = {
	imageUrl:PropTypes.string.isRequired,
	imageWidth:PropTypes.number.isRequired,
	imageHeight:PropTypes.number.isRequired,
	crop:PropTypes.object.isRequired,
	bounds:PropTypes.object.isRequired,
	offset:PropTypes.object.isRequired,
	scaleValue:PropTypes.number,
	imageType:PropTypes.string.isRequired,
	onTypeChange:PropTypes.func.isRequired,
	onDragStart:PropTypes.func.isRequired,
	onDragStop:PropTypes.func.isRequired,
	onScaleChange:PropTypes.func.isRequired,
	showGuide:PropTypes.bool
};
export default Cropper;
