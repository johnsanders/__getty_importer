import React from "react";
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/pro-solid-svg-icons";

const LoadingSpinner = props => (
	<span style={{position:"relative", width:0, height:0}}>
		<Icon
			className="fa-pulse fa-lg"
			icon={faSpinner}
			style={{
				display:props.visible ? "" : "none",
				position:"absolute", 
				left:props.left,
				top:props.top,
				color:"gray"
			}}
		/>
	</span>
);

export default LoadingSpinner;
