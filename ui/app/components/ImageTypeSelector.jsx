import React from "react";
import imageTypes from "../config/imageTypes";

const ImageTypeSelector = props => (
	<div className="row">
		<div className="col-md-12 text-center">
			<div className="checkbox checkbox-primary">
				<label id="imageType" style={{marginRight:"10px"}}>
					Send To:
				</label>
				<select name="imageType" onChange={props.onTypeChange} value={props.imageType}>
					{ 
						Object.values(imageTypes).map( type => (
							<option key={type.name + "Option"}value={type.name}>{type.label}</option> 
						)) 
					}
				</select>
			</div>
		</div>
	</div>
);
export default ImageTypeSelector;
