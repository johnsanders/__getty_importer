import React from "react";
import LoadingSpinner from "./LoadingSpinner";

const SearchInput = props => (
	<form onSubmit={props.handleSearchClick}>
		<div className="form-group">
			<div className="col-sm-4 offset-sm-4 text-center">
				<div className="input-group" style={{display:"inline-flex"}}>
					<input
						id="searchTerm"
						className="form-control"
						value={props.searchTerm}
						onChange={props.handleSearchTermChange}
						placeholder="Search Getty Images"
					/>
					<div className="input-group-append">
						<button className={"btn btn-primary" + (props.loading ? " disabled" : "")}>
							Search
						</button>
					</div>
				</div>
				<LoadingSpinner
					top="0"
					left="20px"
					visible={props.loading}
				/>
			</div>
		</div>
	</form>
);

export default SearchInput;
