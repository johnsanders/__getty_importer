import React from "react";
import PropTypes from "prop-types";

const Notification = props => {
	const styleProp = props.style || {};
	const style = {
		display:"inline-block",
		padding:"5px",
		...styleProp
	};
	const className = props.className || "alert-danger";
	return (
		props.message === "" ? null :
			<div
				className={"alert " + className}
				role="alert"
				style={style}
			>
				<span>
					{props.message}
				</span>
			</div>
	);
};
Notification.propTypes = {
	message:PropTypes.string.isRequired
};
export default Notification;
