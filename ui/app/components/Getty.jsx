import React from "react";
import Nav from "@johnsand/nav";
import Notification from "./Notification";
import LoadingSpinner from "./LoadingSpinner";
import PropTypes from "prop-types";

const Getty = props => (
	<form 
		onSubmit={props.getImage} 
		className="form-horizontal"
	>
		{props.isExtension ? null : <Nav />}
		{props.isExtension ? null : 
			<div className="page-header" style={{marginTop:"80px"}}>
				<h1 className="text-center text-uppercase">GETTY IMPORTER</h1>
			</div>
		}
		<div className="form-group text-center">
			<label className="control-label" htmlFor="id">
				Getty Image ID
			</label>
			<div className="col-sm-4 offset-sm-4">
				<input
					className="form-control"
					type="text"
					name="imageId"
					value={props.imageId}
					onChange={props.onChange}
				/>
			</div>
		</div>
		<div className="form-group text-center">
			<label htmlFor="name" className="control-label">
				Filename tag, so you can find it later. Optional.
			</label>
			<div className="col-sm-4 offset-sm-4">
				<input
					className="form-control"
					name="imageTag"
					type="text"
					value={props.imageTag}
					onChange={props.onChange}
				/>
			</div>
		</div>
		<div className="form-group">
			<div className="col-sm-12 text-center">
				<button
					className={"btn btn-primary" + (props.loading ? " disabled" : "")}
				>
					{props.isExtension ? "Send to CNN" : "Get Image"}
				</button>
				<LoadingSpinner
					left="10px"
					top="0"
					visible={props.loading}
				/>
			</div>
		</div>
		<div className="form-group">
			<div className="col-sm-12 text-center">
				<Notification
					message={props.notificationText}
				/>
			</div>
		</div>
	</form>
);
Getty.propTypes = {
	getImage:PropTypes.func.isRequired,
	imageId:PropTypes.string,
	onChange:PropTypes.func.isRequired,
	imageTag:PropTypes.string,
	loading:PropTypes.bool.isRequired,
	notificationText:PropTypes.string.isRequired
};
export default Getty;
