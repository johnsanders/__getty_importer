import React from "react";

const DragGuide = props => (
	<div
		style={{
			color:"white",
			fontSize:"20px",
			position:"absolute",
			zIndex:150,
			textShadow: "0 0 60px rgb(0,0,0),0 0 40px rgb(0,0,0),0 0 30px rgb(0,0,0),0 0 10px rgb(0,0,0)",
			width:(props.crop.height * props.cropAspect).toString() + "px",
			height:props.crop.height.toString() + "px",
			display:"flex",
			flexWrap:"wrap",
			alignItems:"center",
			justifyContent:"center",
			pointerEvents:"none"
		}}	
	>
		<div
			style={{flex:"0 0 100%"}}
			className="text-center guideArrow guideArrowTop"
		>
			&#8593;
		</div>
		<div
			style={{flex:"0 0 10%", left:"15px"}}
			className="text-center guideArrow guideArrowLeft"
		>
			&#8592;	
		</div>
		<div
			style={{flex:"0 0 80%"}}
			className="text-center"
		>
			<p>Drag this image to position the best crop.</p>
			<p>Use the zoom slider to enlarge if needed.</p>
		</div>
		<div
			style={{flex:"0 0 10%"}}
			className="text-center guideArrow guideArrowRight"
		>
			&#8594;
		</div>
		<div
			style={{flex:"0 0 100%", top:"15px"}} 
			className="text-center guideArrow guideArrowBottom"
		>
			&#8595;
		</div>
	</div>
);

export default DragGuide;
