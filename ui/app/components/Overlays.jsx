import React from "react";
import imageTypes from "../config/imageTypes";

const overlays = {};
Object.values(imageTypes).forEach( imageType => {
	if (imageType.overlay) {
		const overlayFilename = imageType.name + "_overlay.png";
		import("../img/" + overlayFilename)
			.then( img => {
				overlays[imageType.name] = img;
			})
	}
}, {});


const Overlays = props => {
	const {imageType, crop, cropAspect} = props;
	if (overlays[imageType]) {
		return (
			<img
				src={overlays[imageType].default}
				style={{
					position:"absolute",
					zIndex:100,
					width:(crop.height * cropAspect).toString() + "px",
					height:crop.height.toString() + "px",
					pointerEvents:"none"
				}}
			/>
		);
	} else {
		return null;
	}
};
export default Overlays;
