import React from "react";
import autobind from "react-autobind";
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faCopy, faForward} from "@fortawesome/fontawesome-pro-solid";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {SortableContainer, SortableElement, arrayMove} from "react-sortable-hoc";

const imgHeight = "80px";
const SortableFavorite = SortableElement(props =>
	<div className="card m-1" key={"fav-react-" + props.fav.id}>
		<div className="px-2 flex-apart">
			<span>{props.fav.id}</span>
			<button
				className="searchFavoriteClose close"
				data-id={props.fav.id}
				onClick={props.handleDelete}
			>
				&times;
			</button>
		</div>
		<img src={props.fav.cnn_thumb} height={props.imgHeight} />
	</div>
);
const SortableFavorites = SortableContainer(props =>
	<div className="searchFavoritesList">
		{
			props.favorites.map((fav, i) => (
				<SortableFavorite
					key={"fav-sortable-" + fav.id}
					index={i}
					fav={fav}
					imgHeight={props.imgHeight}
					handleDelete={props.handleDelete}
				/>
			))
		}
	</div>
);

class SearchFavoritePanel extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.state = {clipboardTooltip:"Copy to Clipboard", favoriteList:""};
	}
	componentDidUpdate(prevProps) {
		if (this.props.favorites.some((fav, i) => fav !== prevProps.favorites[i])) {
			const list = this.props.favorites.reduce((acc, fav) => acc + " " + fav.id, "");
			this.setState({favoriteList:list});
		}
	}
	handleSortEnd({oldIndex, newIndex}) {
		this.props.updateFavorites(arrayMove(this.props.favorites, oldIndex, newIndex));
	}
	handleDelete(e) {
		const id = e.currentTarget.getAttribute("data-id");
		console.log(id)
		const newFavs = this.props.favorites.filter(fav => fav.id !== id);
		this.props.updateFavorites(newFavs);
	}
	render() {
		return (
			this.props.favorites.length === 0 ? null :
				<div className="searchFavorites card">
					<div className="searchFavoritesHeader card-header flex-apart">
						<span className="mx-3">FAVORITES</span>
						<div className="btn-group">
							<CopyToClipboard text={this.state.favoriteList} onCopy={this.handleCopy}>
								<button
									className="btn btn-secondary btn-sm"
									data-tip="Copy Getty Numbers to Clipboard"
								>
									<Icon icon={faCopy} />
								</button>
							</CopyToClipboard>
							<button className="btn btn-secondary btn-sm" data-tip="Send All to Stillstring">
								<Icon icon={faForward} />
							</button>
						</div>
					</div>
					<SortableFavorites
						favorites={this.props.favorites}
						imgHeight={imgHeight}
						onSortEnd={this.handleSortEnd}
						handleDelete={this.handleDelete}
						lockToContainerEdges={true}
						axis="x"
						lockAxis="x"
						helperClass="sortableHelper"
					/>
				</div>
		);
	}
}
export default SearchFavoritePanel;
