import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import qs from "qs";
import Edit from "./Edit";
import Nav from "@johnsand/nav";
import Results from "./Results";
import imageTypes from "../config/imageTypes";
import PropTypes from "prop-types";

class EditContainer extends React.Component {
	constructor(props) {
		super(props);
		const {extension} = qs.parse(this.props.location.search, {ignoreQueryPrefix:true});
		this.isExtension = extension === "1";
		this.state = {
			imageType:"newsimage",
			resultsReady:false, 
			loading:false
		};
		autobind(this);
	}
	componentWillUnmount() {
		this.setState({resultsReady:false});
	}
	onCropUpdate(x, y, scale, imageType) {
		this.cropX = x;
		this.cropY = y;
		this.scale = scale;
		this.imageType = imageType;
	}
	onDestinationChange(e) {
		this.setState({[e.target.id + "Send"]:!this.state[e.target.id + "Send"]});
	}
	handleImageTypeChange(newType) {
		return new Promise( (resolve, reject) => {
			this.setState({imageType:newType}, () => resolve() );
		});
	}
	onSubmit() {
		if (this.state.loading) { return; }
		this.setState({loading:true});
		const params = new URLSearchParams();
		params.append("filename", this.props.match.params.filename);
		params.append("cropX", this.cropX);
		params.append("cropY", this.cropY);
		params.append("scale", this.scale);
		params.append("imageType", this.imageType);
		axios.post("/utilities/getty_importer/edit_image_2.php", params)
			.then(response => {
				this.setState({resultsReady:true, loading:false});
			});
	}
	render() {
		return (
			<div>
				{this.isExtension ? null :
					<Nav />
				}
				{this.state.resultsReady ?
					<Results
						imageUrl={"/utilities/getty_importer/tmp/crop_" + this.props.match.params.filename}
						destinations = {this.state.resultDestinations}
					/>
				:
					<Edit
						touchscreenSend={this.state.touchscreenSend}
						imageType={this.state.imageType}
						handleImageTypeChange={this.handleImageTypeChange}
						onSubmit={this.onSubmit}
						onCropUpdate={this.onCropUpdate}
						imageFilename={this.props.match.params.filename}
						imageWidth={this.props.match.params.width}
						imageHeight={this.props.match.params.height}
						loading={this.state.loading}
						isExtension={this.isExtension}
					/>
			}
			</div>
		);
	}
}
EditContainer.propTypes = {
	match:PropTypes.object.isRequired
};
export default EditContainer;
