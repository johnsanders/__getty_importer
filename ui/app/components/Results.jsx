import React from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const Results = props => (
	<div className="row" style={{marginTop:"70px"}}>
		<div className="col-sm-8 offset-sm-2 text-center">
			<h2>All done.</h2>
			<div className="form-group">
				<img className="resultImage" src={props.imageUrl}/>
			</div>
			<div className="form-group">
				<div className="col-sm-12">
					<a
						className="btn btn-primary"
						download={true}
						href={props.imageUrl}
					>
						Download as JPEG
					</a>
					<Link
						style={{marginLeft:"10px"}}
						to="/find"
						className="btn btn-default"
					>
						Import Another
					</Link>
				</div>
			</div>
		</div>
	</div>
);
Results.propTypes = {
	imageUrl:PropTypes.string.isRequired
};
export default Results;
