import React from "react";
import autobind from "react-autobind";
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/pro-solid-svg-icons";

class SearchResultImage extends React.Component {
	constructor(props){
		super(props);
		autobind(this);
		this.state = {imageLoaded:false};
	}
	handleImageLoad(){
		this.setState({imageLoaded:true});
	}
	render(){
		const {result} = this.props;
		return (
			<div 
				onMouseOver={this.props.handleMouseOver}
				onMouseOut={this.props.handleMouseOver}
				data-id={result.id}
			>
				<img
					className="card-img-top p-2" 			 
					style={{objectFit:"contain", height:"150px"}}
					src={result.cnn_thumb}
					onLoad={this.handleImageLoad}
				/>
				<Icon
					className="fa-pulse fa-lg"
					icon={faSpinner}
					style={{
						position:"absolute",
						top:"30%",
						left:"50%",
						display:this.state.imageLoaded ? "none" : ""
					}}
				/>
			</div>
		);
	}
}
export default SearchResultImage;
