import React from "react";
import axios from "axios";
import autobind from "react-autobind";
import filenamify from "filenamify";
import imageTypes from "../config/imageTypes";
import CropperContainer from "./CropperContainer";
import LoadingSpinner from "./LoadingSpinner";

const SearchCropperWindow = props => (
	<div className="cropperWindow">
		<div>
			<h2 style={{marginBottom:"30px"}}>SET IMAGE CROP</h2>
			<CropperContainer 
				onCropUpdate={props.onCropUpdate}
				imageType={props.imageType}
				handleImageTypeChange={props.handleImageTypeChange}
				imageUrl={props.imageUrl}
				width={props.imageWidth.toString()}
				height={props.imageHeight.toString()}
			/>
			<span>
				<button
					className="btn btn-secondary mr-2"
					onClick={props.handleCancel}
				>
					Cancel
				</button>
				<button
					className="btn btn-primary"
					onClick={props.downloadAndCrop}
				>
					Done
				</button>
				<LoadingSpinner
					top="0"
					left="15px"	
					visible={props.loading}
				/>
			</span>
		</div>
	</div>
);

export default SearchCropperWindow;
