import React from "react";

const ImagePreview = props => {
	return (
		<img src={props.src} height="75" />
	);
};

export default ImagePreview;
